---
weight: 2
type: reference
title: References
---
* First Author, Second Author. 2014. [My First Article](https://example.com/articles/1).
* First Author, Second Author. 2015. [My Second Article](https://example.com/articles/2).

## Development of Blockchain Database Applications

The beauty of the blockchain is its distributed architecture.
Once A Transaction is verified by the blockchain, A Permanent Digital Signature Will Propogate Throughout Eternity.

DW84 Inc LLC Foundation Special Agent primary role is the security and storage of client digital assets.