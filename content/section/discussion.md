---
title: "Discussion"
weight: 5
---
## BLOCKCHAIN TRANSACTIONS

Transactions make changes to storage and have fixed transaction costs.
Transactions are executed on every node.
Transactions can only return results by events.
One Contract Transact On Another and get returned data.
TRANSACTION FAILURE ALL CHANGES REVERSED, NO LOG PRODUCED

Transactions can POST a Contract
Another transaction can call that contract.

## DIGITAL SIGNATURES

Hashing is the product of a small change in content which causes a huge change in results.

Public Key Encryption is the method to encrypt hash values.