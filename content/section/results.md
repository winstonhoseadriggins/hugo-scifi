---
title: "Results"
weight: 4
---
The results of the experiment. See [Figure 1](#figure-1) and [Figure 2](#figure-2), as well as [Table 1](#table-1), for details.

## CRYPTOCURRENCY 

Propogate and Preserve Your Digital Assets With Blockchain Technologies.

DW84 Inc LLC Foundation Acquisition Agent primary
role is the integration of "smart contracts" with 
client digital assets.